# WebTesting FranGuerrero

## Description
This is a project including the resolution of a technical test as part of a hiring process. The files included in this project are aimed to solve the implementation of automated Web test cases for Google Search page using Cypress and Cucumber frameworks as required.


To make it easy for you to get started, here's a list of recommended next steps.

## Pre-requirements
- [ ] [Have Node.js downloaded and installed in you local machine](https://nodejs.org/es/download)

## Getting started and Installation
- [ ] [Clone the project into your local machine]
```
git clone https://gitlab.com/fran.guerrero.sanchez/webtestingfranguerrero.git

```
- [ ] [Install dependencies including Axios and Cucumber]
```
npm i 

```
## Test in Local Machine
- [ ] [Execute API Tests]
```
npm run test

```
## Test using Docker Container

- [ ] [Have Docker downloaded and installed](https://docs.docker.com/get-docker/)
- [ ] [Build and execute the image. This will automatically launch API tests within Docker container]
```
docker build -t googlewebtesting .

```
```
docker run -it googlewebtesting

```

***

# Test using GitlabCI Pipeline
- [ ] [Fork this repository](https://gitlab.com/fran.guerrero.sanchez/webtestingfranguerrero.git)
- [ ] [Click Build/Pipelines section under left hand navigation menu]
- [ ] [Click to Run the Pipeline]


## Project Name
Cypress+Cucumber Web Tests


## Project Author
Fran Guerrero
